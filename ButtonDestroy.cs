﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ButtonDestroy : MonoBehaviour
{
    ButtonToolTip toolTip;
    void Start()
    {
        toolTip = FindObjectOfType<ButtonToolTip>();
        toolTip.gameObject.SetActive(false);
    }

    private void OnMouseOver()
    {
        toolTip.gameObject.SetActive(true);
        toolTip.GetComponent<Text>().text = "Destroy selected tower";
    }

    private void OnMouseExit()
    {
        toolTip.gameObject.SetActive(false);
    }
}
