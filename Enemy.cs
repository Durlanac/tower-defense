﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{
    [SerializeField] Enemy enemyPrefab;
    int health = 280;
    string projectileName;
    // Start is called before the first frame update
    void Start()
    {
        var enemyHealthGrow = EnemySpawner.FindObjectOfType<EnemySpawner>().GetWaveIndex();
        health += health * enemyHealthGrow / 5;
    }

    void Update()
    {
    
    }

    public int GetEnemyHealth(GameObject other)
    {
        if (other.tag == "Respawn")
        {
            projectileName = other.name;
        }
        if (projectileName.Contains("Blue"))
        {
            var projectile = FindObjectOfType<Projectile>();
            var damageSource = projectile.GetDamageSource();
            health -= damageSource;
        }
        else if (projectileName.Contains("Red"))
        {
            var projectile = FindObjectOfType<ProjectileRed>();
            var damageSource = projectile.GetDamageSource();
            health -= damageSource;
        }
        else if (projectileName.Contains("Green"))
        {
            var projectile = FindObjectOfType<ProjectileGreen>();
            var damageSource = projectile.GetDamageSource();
            health -= damageSource;
        }

        return health;
    }

    private void OnTriggerEnter2D(Collider2D otherCollider)
    {
        {
            if (otherCollider.tag == "Respawn" )
            {
                var enemyHealth = GetEnemyHealth(otherCollider.gameObject);
                
                Debug.Log(enemyHealth);
                Destroy(otherCollider.gameObject);
                
                if (enemyHealth <= 0)
                {
                    Destroy(gameObject);
                }
            }
        }
    }
}
