﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Tower : MonoBehaviour
{
    [SerializeField] Shooter shooterPrefab;

    private void OnMouseDown()
    {
        var towers = FindObjectsOfType<Tower>();
        foreach (var tower in towers)
        {
            tower.GetComponent<SpriteRenderer>().color = new Color32(130, 130, 130, 255);
        }
        GetComponent<SpriteRenderer>().color = Color.white;
        FindObjectOfType<BuildZone>().SetSelectedDefender(shooterPrefab);

        if (gameObject.name.Contains("Red"))
        {
            var text = BackgroundText.FindObjectOfType<BackgroundText>();
            text.GetComponent<Text>().text = "Red Tower\n Damage: 70 Cost: 150";
        }
        else if (gameObject.name.Contains("Green"))
        {
            var text = BackgroundText.FindObjectOfType<BackgroundText>();
            text.GetComponent<Text>().text = "Green Tower\n Damage: 100 Cost: 200";
        }
        else
        {
            var text = BackgroundText.FindObjectOfType<BackgroundText>();
            text.GetComponent<Text>().text = "Stone Tower\n Damage: 50 Cost: 100";
        }
    }
}
