﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProjectileRed : MonoBehaviour
{
    float speed = 5f;
    int damage = 70;
    Vector3 projectilePoint = new Vector3();
    Enemy targetedEnemy;
    Enemy[] enemies;
    int[] enemyDistances;

    void Start()
    {
        StartCoroutine(StartProjectileFixCoroutine());
        GetEnemyTarget();
    }

    void Update()
    {
        if (targetedEnemy != null)
        {
            transform.position =
            Vector3.MoveTowards(transform.position,
            targetedEnemy.transform.position, speed * Time.deltaTime);
        }
    }

    private IEnumerator StartProjectileFixCoroutine()
    {
        yield return StartCoroutine(ProjectileFixCoroutine());
    }
    public IEnumerator ProjectileFixCoroutine()
    {
        yield return new WaitForSeconds(1);

        projectilePoint = transform.position;

        if (projectilePoint.Equals(transform.position))
        {
            Destroy(gameObject);
        }

        yield return StartCoroutine(StartProjectileFixCoroutine());
    }

    public int GetDamageSource()
    {
        return damage;
    }

    public float GetSpeed()
    {
        return speed;
    }

    private void GetEnemyTarget()
    {
        enemies = FindObjectsOfType<Enemy>();
        enemyDistances = new int[enemies.Length];

        for (int i = 0; i < enemies.Length; i++)
        {
            {
                enemyDistances[i] = (int)
                    Vector2.Distance(transform.position, enemies[i].transform.position);
            }
        }

        var minimumDistanceTarget = Min();

        for (int i = 0; i < enemies.Length; i++)
        {
            if ((int)Vector2.Distance(transform.position, enemies[i].transform.position) == minimumDistanceTarget)
            {
                targetedEnemy = enemies[i];
            }
        }
    }

    private int Min()
    {
        int Min = 999;

        foreach (int item in enemyDistances)
        {
            if (item < Min)
            {
                Min = item;
            }
        }

        return Min;
    }
}
