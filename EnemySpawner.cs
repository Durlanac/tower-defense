﻿using System.Collections;
using System.Collections.Generic;
using System.Threading;
using UnityEngine;

public class EnemySpawner : MonoBehaviour
{
    [SerializeField] List<WaveConfig> waveConfigs;
    EnemySpawner enemies;
    int waveIndex = 0;
    bool nextLevel = true;
    int countdown = 0;

    void Start()
    {
        enemies = FindObjectOfType<EnemySpawner>();
    }

    
    private void SpawnAllWaves()
    {
        if (nextLevel)
        {
            var shooters = FindObjectsOfType<Shooter>();
            foreach (var item in shooters)
            {
                item.animator.enabled = false;
            }

            countdown = 0;
            Thread t = new Thread(StartWave);
            void StartWave()
            {
                for (int i = 0; i < 5; i++)
                {
                    Thread.Sleep(1000);
                    countdown++;
                    Debug.Log(countdown);
                }

            }
            t.Start();
        }
        
        nextLevel = false;
        if (countdown >= 5)
        {
            for (; this.waveIndex < waveConfigs.Count;)
            {
                var currentWave = waveConfigs[this.waveIndex];

                StartCoroutine(SpawnAllEnemiesInWave(currentWave));

                break;
            }
            this.waveIndex++;
            nextLevel = true;
        }
    }

    private IEnumerator SpawnAllEnemiesInWave(WaveConfig waveConfig)
    {
        for (int enemyCount = 0; enemyCount < waveConfig.GetNumberOfEnemies(); enemyCount++)
        {
            var newEnemy = 
            Instantiate(waveConfig.GetEnemyPrefab(), waveConfig.GetWaypoints()[0].transform.position, Quaternion.identity);
            newEnemy.GetComponent<EnemyPathing>().SetWaveConfig(waveConfig);
            newEnemy.transform.parent = transform;

            yield return new WaitForSeconds(waveConfig.GetTimeBetweenSpawns());
        }
    }

    void Update()
    {
        if (enemies.transform.childCount <= 0)
        {
            SpawnAllWaves();
        }
    }

    public int GetWaveIndex()
    {
        return waveIndex;
    }
}