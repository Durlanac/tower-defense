﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Button : MonoBehaviour
{
    public BuildZone BZ;
    int countButtonClick = 0;
    bool buildCheck = false;
    Buildings buildings;
    public Shooter towerToDestroy;
    public Shooter towerToUpgrade;
    public Vector3 zoneWhereToUpgrade;
    TowerUpgradeSystem TUS;
    Tower t;

    void Start()
    {
        BZ = FindObjectOfType<BuildZone>();
        buildings = FindObjectOfType<Buildings>();
        buildings.gameObject.SetActive(false);
        TUS = FindObjectOfType<TowerUpgradeSystem>();
    }

    void Update()
    {
        
    }

    public void Build_Click()
    {
        countButtonClick++;
        if(countButtonClick > 0 && countButtonClick < 2)
        {
            buildings.gameObject.SetActive(true);
            buildCheck = true;
            foreach (var region in BZ.regions)
            {
                region.GetComponent<Image>().color = new Color32(141, 255, 169, 50);
            }
        }
        else
        {
            buildings.gameObject.SetActive(false);
            buildCheck = false;
            countButtonClick = 0;
            foreach (var region in BZ.regions)
            {
                region.GetComponent<Image>().color = new Color32(141, 255, 169, 0);
            }
        }
    }

    public void Destroy_Click()
    {
        if (towerToDestroy == null)
        {
            Debug.Log("Select tower to destroy");
            return;
        }
            
        Destroy(towerToDestroy.gameObject);
    }

    public void Upgrade_Click()
    {
        if (towerToDestroy == null)
        {
            Debug.Log("Select tower to upgrade");
            return;
        }

        bool canUpgrade = true;
        Sprite newUpgrade = null;

        if (towerToDestroy.name.Contains("Red"))
        {
            
        }
        else if (towerToDestroy.currentTowerLevel < 3)
        {
            newUpgrade = Resources.Load("Stone Tower/Stone Tower Level " + ++towerToDestroy.currentTowerLevel, typeof(Sprite)) as Sprite;
            towerToDestroy.towerUpgradeDamage += 50;
        }
        else
        {
            canUpgrade = false;
            Debug.Log("Tower upgrade is maximum level");
        }

        if (canUpgrade)
        {
            towerToDestroy.GetComponent<SpriteRenderer>().sprite = newUpgrade;
        }
    }


    private IEnumerator Upgrade() 
    {
        yield return new WaitForSeconds(0.5f);
        
    }

    public bool GetBuildCkeck()
    {
        return buildCheck;
    }
}
