﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shooter : MonoBehaviour
{
    [SerializeField] GameObject projectilePrefab;
    public Animator animator;
    bool inRange = false;
    public int currentTowerLevel = 1;
    public int towerUpgradeDamage = 0;

    void Start()
    {
        animator = GetComponent<Animator>();
    }

    private void OnTriggerStay2D(Collider2D other)
    {
        if (other.tag == "Player")
        {
            inRange = true;
        }
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "Player")
        {
            animator.enabled = true;
        }

        if (other.tag == "Respawn")
        {

        }
    }

    void OnTriggerExit2D(Collider2D other)
    {
        if (other.tag == "Player")
        {
            inRange = false;
        }
    }

    void Update()
    {
        if (inRange)
        {
            animator.SetBool("isInRange", true);
        }
        else
        {
            animator.SetBool("isInRange", false);
        }
    }
    
    public void Fire()
    {
        var newProjectile =
        Instantiate(projectilePrefab, transform.position, transform.rotation);
        newProjectile.transform.parent = transform;
    }
}
