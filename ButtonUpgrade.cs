﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ButtonUpgrade : MonoBehaviour
{
    ButtonToolTip toolTip;
    void Start()
    {
        toolTip = FindObjectOfType<ButtonToolTip>();
    }

    private void OnMouseOver()
    {
        toolTip.gameObject.SetActive(true);
        toolTip.GetComponent<Text>().text = "Upgrade selected tower";
    }

    private void OnMouseExit()
    {
        toolTip.gameObject.SetActive(false);
    }
}
