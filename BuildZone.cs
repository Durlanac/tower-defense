﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BuildZone : MonoBehaviour
{
    [SerializeField] public RangeImage imagePrefab;
    public Shooter towerToBuild;
    public BuildZone[] regions;
    Button button;
    Shooter newTower;
    public RangeImage newRangeImage;
    public int dicCount = 0;
    public int spawnProjectileCount = 0;
    bool unclickCheck = false;
    Shooter towerToDestroy;
    public Dictionary<Collider2D, int> enemyData = new Dictionary<Collider2D, int>();

    void Start()
    {
        regions = FindObjectsOfType<BuildZone>();
        button = FindObjectOfType<Button>();
    }

    public void SetSelectedDefender(Shooter towerToBuild)
    {
        foreach (var item in regions)
        {
            item.towerToBuild = towerToBuild;
        }
    }

    private void OnMouseDown()
    {
        if (button.GetBuildCkeck())
        {
            BuildTower(GetMousePosition());
        }
        else if (transform.childCount == 1 && !newRangeImage.gameObject.active)
        {
            var countRangeImages = FindObjectsOfType<RangeImage>();
            foreach (var image in countRangeImages)
            {
                if (image.gameObject.active)
                image.gameObject.SetActive(false);
            }
            newRangeImage.gameObject.SetActive(true);
            unclickCheck = true;
            button.towerToDestroy = transform.GetChild(0).GetComponent<Shooter>();
            button.zoneWhereToUpgrade = transform.position;
            button.BZ = transform.GetComponent<BuildZone>();
        }
        else if (unclickCheck)
        {
            unclickCheck = false;
            newRangeImage.gameObject.SetActive(false);
            button.towerToDestroy = null;
            //button.zoneWhereToUpgrade = null;
        }
    }

    private Vector2 GetMousePosition()
    {
        Vector2 clickPos = new Vector2(transform.position.x, transform.position.y);
        return clickPos;
    }

    private void BuildTower(Vector2 position)
    {
        
        if (transform.childCount < 1)
        {
            newTower =
            Instantiate(this.towerToBuild, position, Quaternion.identity)
            as Shooter;
            newTower.transform.SetParent(transform);
            newRangeImage =
                Instantiate(imagePrefab, newTower.transform.position, Quaternion.identity)
            as RangeImage;
            newRangeImage.transform.SetParent(newTower.transform);
            newRangeImage.gameObject.SetActive(false);
            //Debug.LogError("IMAGE CREATED");
        }
    }

    public Shooter GetTowerToDestroy() 
    {
        return towerToDestroy;
    }

    private void OnMouseOver()
    {
        if(transform.childCount > 0)
        {
            transform.GetChild(0).GetComponent<SpriteRenderer>().color = new Color32(100, 255, 100, 255);
            return;
        }
    }

    private void OnMouseExit()
    {
        if (transform.childCount > 0)
        {
            transform.GetChild(0).GetComponent<SpriteRenderer>().color = new Color32(255, 255, 255, 255);
            return;
        }
    }

    void Update()
    {

    }

    
}
